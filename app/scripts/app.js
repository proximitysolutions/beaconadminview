'use strict';
/**
 * @ngdoc overview
 * @name upphotoviewApp
 * @description
 * # upphotoviewApp
 *
 * Main module of the application.
 */
angular.module('myApp', [
    'ngResource',
    'ngRoute',
    'ngCookies',
    'ui.bootstrap',
    'smart-table'

])
	.config(function ($routeProvider) {
	    $routeProvider
		    .when('/home', {
			templateUrl: 'views/main.html',
			controller: 'MainCtrl',
			activetab: 'home'
		    })
                    .when('/beacons', {
			templateUrl: 'views/beaconlist.html',
			controller: 'BeaconListCtrl',
			activetab: 'beacons'
		    })
                    .when('/beacons/:beaconId', {
			templateUrl: 'views/beacondetail.html',
			controller: 'BeaconDetailCtrl',
			activetab: 'beacons'
		    })
                    .when('/beacon/advertises/:beaconId', {
			templateUrl: 'views/beacon2advertisements.html',
			controller: 'Beacon2AdvertisementsCtrl',
			activetab: 'beacons'
		    })
                    .when('/apps', {
			templateUrl: 'views/applist.html',
			controller: 'AppListCtrl',
			activetab: 'apps'
		    })
                    .when('/partners', {
			templateUrl: 'views/partnerlist.html',
			controller: 'PartnerListCtrl',
			activetab: 'partners'
		    })
                    .when('/partners/:partnerId', {
			templateUrl: 'views/partnerdetail.html',
			controller: 'PartnerDetailCtrl',
			activetab: 'partners'
		    })
//                    .when('/advertises', {
//			templateUrl: 'views/advertisementlist.html',
//			controller: 'AdvertisementListCtrl',
//			activetab: 'advertisements'
//		    })
		    .when('/doors', {
			templateUrl: 'views/doorlist.html',
			controller: 'DoorListCtrl',
			activetab: 'doors'
		    })
//		    .when('/users', {
//			templateUrl: 'views/userlist.html',
//			controller: 'UserListCtrl',
//			activetab: 'users'
//		    })
		    .when('/login', {
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl',
			activetab: 'login'
		    })
//		    .when('/groups', {
//			templateUrl: 'views/grouplist.html',
//			controller: 'GroupListCtrl',
//			activetab: 'groups'
//		    })
		    .when('/errorCMD', {
			templateUrl: 'views/errorcmd.html',
			controller: 'ErrorcmdCtrl'
		    })
		    .otherwise({
			redirectTo: '/home'
		    });
	})

	.controller('IndexCtrl', ['$scope', '$cookieStore', '$modal', '$window',
	    'SessionService', 'ErrorCMDService', 'UserService',
	    function ($scope, $cookieStore, $modal, $window,
		    SessionService, ErrorCMDService, UserService) {

		$scope.showLogin = function () {
		    $modal.open({
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl'
		    });
		};
		$scope.isLoggedIn = function () {
		    return SessionService.getUser() !== undefined;
		};
		$scope.logOut = function () {
		    SessionService.logOut();
		};
		$scope.getProfile = function () {
		    var resp = UserService.getUserBySession({
		    }, function () {
			if (resp.error_code === 0)
			{
			    $modal.open({
				templateUrl: 'views/userdetail.html',
				controller: 'UserDetailCtrl',
				resolve: {
				    cb: function () {
					return undefined;
				    },
				    user: function () {
					return resp.data;
				    }
				}
			    });
			}
			else
			{
			    ErrorCMDService.displayError('No Permission');
			}
		    });
		};
		$scope.getUserMenu = function ()
		{
		    return $cookieStore.get('user');
		};
		$scope.userMenu = $scope.getUserMenu();
		$scope.getMenuTabs = function () {

		    var tabs;
		    tabs = [{name: 'home', path: '#/home', title: 'Home'}];
                    tabs.push({name: 'beacons', path: '#/beacons', title: 'iBeacons'});
                    tabs.push({name: 'apps', path: '#/apps', title: 'Apps'});
                    tabs.push({name: 'partners', path: '#/partners', title: 'Partners'});
                    //tabs.push({name: 'advertisements', path: '#/advertises', title: 'Advertisements'});
		    //tabs.push({name: 'doors', path: '#/doors', title: 'Doors'});
		    //tabs.push({name: 'users', path: '#/users', title: 'Users'});
		    //tabs.push({name: 'groups', path: '#/groups', title: 'Groups'});

		    return tabs;
		};
		$scope.menutabs = $scope.getMenuTabs();
	    }])
	;
