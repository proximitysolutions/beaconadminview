/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';
var app = angular.module('myApp');
app.controller('AppDetailCtrl', ['$scope', '$modalInstance', 'app', 'cb',
    'AppService', 'ErrorCMDService',
    function ($scope, $modalInstance, app, cb,
	    AppService, ErrorCMDService) {

	$scope.hasApp = (app !== undefined);
	$scope.init = function ()
	{
	    $scope.newApp = {name: 'name', secretKey: '', urlCall: '', enable: 1};
	    if ($scope.hasApp)
	    {
		$scope.localApp = {
                    id: app.appId,
		    name: app.appName,
                    secretKey: app.secretKey,
                    urlCall: app.urlCall,
                    enable: app.enable
		};
	    }


	};
	$scope.init();


	$scope.ok = function () {
	    $modalInstance.close($scope.localApp);
	};
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

	$scope.msgList = [];
	$scope.clearMsg = function () {
	    $scope.msgList = [];
	};
	$scope.showMsg = function (msg) {
	    $scope.msgList = [{type: 'danger', content: msg}];
	};


	$scope.confirm = function () {

	    if ($scope.hasApp)
	    {
		var resp = AppService.updateApp({
                    appId: $scope.localApp.id,
		    name: $scope.localApp.name,
		    url: $scope.localApp.urlCall,
                    enable: $scope.localApp.enable
		}, function () {
		    if (resp.error === 0)
		    {
			$scope.ok();
			cb();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.error);
		    }

		});
	    }
	    else
	    {
		var resp = AppService.addApp({
		    name: $scope.newApp.name,
		    url: $scope.newApp.urlCall,
                    enable: $scope.newApp.enable

		}, function () {
		    if (resp.error === 0) {
			$scope.ok();
			cb();
		    }
		    else {
			$scope.showMsg('Add failed');
		    }

		});
	    }
	}
	;
    }
]);
