/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';
var app = angular.module('myApp');
app.controller('Beacon2AdvertisementsCtrl', ['$cookieStore', '$scope', '$modal', '$route', '$routeParams',
    'BeaconService', 'AdvertisementService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route, $routeParams,
            BeaconService, AdvertisementService, ErrorCMDService, SessionService) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;
        $scope.beaconId = $routeParams.beaconId;
        
        $scope.addAdvertisement = function () {
            $modal.open({
                templateUrl: 'views/addadvertisementtobeacon.html',
                controller: 'AddAdvertisementToBeaconCtrl',
                resolve: {
                    beaconId: function () {
                        return $scope.beaconId;
                    },
                    cb: function () {
                        return $scope.pageChanged;
                    }
                }
            });
        };
        
        $scope.removeAdvertisement = function (adId)
        {

            var cb = function () {
                var resp = AdvertisementService.removeAdvertisementsFromBeacon({
                    beaconId: $scope.beaconId,
                    adIds: adId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        //ErrorCMDService.displayError(resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

        $scope.pageChanged = function ()
        {
            //var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = AdvertisementService.listAdvertisementFromBeacon({
                beaconId: $scope.beaconId

            }, function () {

                if (apiResp.error === 0) {
                    var advertisements = apiResp.data;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.advertisementList = advertisements;
                    $scope.advertisements = [].concat($scope.advertisementList);
                    //$scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
//                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    //ErrorCMDService.displayError(apiResp.message);
                }
            });
        };

        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
//	$scope.setPage = function (pageNo) {
//	    $scope.currentPage = pageNo;
//	};
        $scope.init = function () {

            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            
            $scope.updateDisplay();
        };
        $scope.init();
    }
]);
