'use strict';
var app = angular.module('myApp');
app.controller('BeaconDetailCtrl', ['$cookieStore', '$scope', '$modal', '$route', '$routeParams',
    'BeaconService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route, $routeParams,
            BeaconService, ErrorCMDService, SessionService) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;
        $scope.showPartnerList = true;
        //$scope.beaconId = -1;
        $scope.beaconId = $routeParams.beaconId;
        
        $scope.getBeacon2AdvertisesUri = function ()
        {
            return '#beacon/advertises/' + $scope.beaconId;
        };
        
        $scope.getBeaconDetail = function (beaconId)
        {
            var resp = BeaconService.getBeacon({
                beaconId: beaconId
            }, function ()
            {
                if (resp.error === 0)
                {
                    $scope.currentBeacon = resp.data;
                }
                else
                {
                    //ErrorCMDService.displayError(resp.message);
                }

            });
        };

        $scope.removePartnerFromBeacon = function (partnerId)
        {

            var cb = function () {
                var resp = BeaconService.removePartner({
                    beaconId: $scope.beaconId,
                    partnerId: partnerId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        ErrorCMDService.displayError(resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

//        $scope.backToBeaconList = function ()
//        {
//            $scope.beaconId = -1;
//            $scope.showPartnerList = false;
//            $scope.pageChanged();
//        };

        $scope.updateBeaconInfo = function () {
            var resp = BeaconService.addBeacon({
                name: $scope.currentBeacon.beaconName,
                description: $scope.currentBeacon.description,
                uuid: $scope.currentBeacon.uuid,
                major: $scope.currentBeacon.bMajor,
                minor: $scope.currentBeacon.bMinor,
                lat: $scope.currentBeacon.lat,
                lng: $scope.currentBeacon.lng,
                enable: $scope.currentBeacon.enable

            }, function () {
                if (resp.error === 0) {
                    //$scope.ok();
                    //cb();
                    alert("Update thành công");
                }
                else {
                    alert("Update thất bại\n\r message: " + resp.message + " error: " + resp.error);
                }

            });
        };
        $scope.addPartner = function () {

            $modal.open({
                templateUrl: 'views/addpartnertobeacon2.html',
                controller: 'PartnerListCtrl2',
                resolve: {
                    beaconId: function () {
                        return $scope.beaconId;
                    },
                    cb: function () {
                        return $scope.partnerPageChanged;
                    }
                }
            });
        };

        $scope.pageChanged = function ()
        {
            if ($scope.showPartnerList) {
                $scope.partnerPageChanged();
                return;
            }
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = BeaconService.listBeacon({
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var beacons = apiResp.data.beacons;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.beaconList = beacons;
                    $scope.beacons = [].concat($scope.beaconList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    ErrorCMDService.displayError(apiResp.message);
                }
            });
        };
        $scope.partnerPageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = BeaconService.listPartner({
                beaconId: $scope.beaconId,
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var partners = apiResp.data.partners;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.partnerList = partners;
                    $scope.partners = [].concat($scope.partnerList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    ErrorCMDService.displayError(apiResp.message);
                }
            });
        };

        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
//	$scope.setPage = function (pageNo) {
//	    $scope.currentPage = pageNo;
//	};
        $scope.init = function () {

            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            var sessionId = SessionService.getSessionId();
            if (sessionId)
            {
                $scope.updateDisplay();
            } else {
                $scope.updateDisplay();
            }
            $scope.getBeaconDetail($scope.beaconId);
        };
        $scope.init();
    }
]);