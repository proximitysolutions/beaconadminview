/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('BeaconsFromPartnerCtrl', ['$scope', '$modal', 'partnerId', 'cb', '$route',
    'PartnerService', 'BeaconService', 'ErrorCMDService', 'SessionService',
    function ($scope, $modal, partnerId, cb, $route,
	    PartnerService, BeaconService, ErrorCMDService, SessionService) {

//	$scope.getAdmin = function ()
//	{
//	    //return $cookieStore.get('user').isAdmin;
//            return true;
//	};
//
//	//$scope.isAdmin = $scope.getAdmin();
//        $scope.isAdmin = true;


	$scope.addBeacon = function () {
            
	    $modal.open({
		templateUrl: 'views/addbeacontopartner.html',
		controller: 'AddBeaconToPartnerCtrl',
		resolve: {
		    partnerId: function () {
			return partnerId;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};
	$scope.viewBeaconDetail = function (beaconId)
	{
	    var resp = BeaconService.getBeacon({
		beaconId: beaconId
	    }, function ()
	    {
		if (resp.error === 0)
		{
		    $modal.open({
			templateUrl: 'views/beacondetail.html',
			controller: 'BeaconDetailCtrl',
			resolve: {
			    beacon: function () {
				var beacon = resp.data;
				beacon.beaconId = beaconId;			
				return beacon;
			    },
			    cb: function () {
				return $scope.updateDisplay;
			    }
			}
		    });
		}
		else
		{
		    ErrorCMDService.displayError(resp.message);
		}

	    });
	};
	$scope.removeBeaconFromPartner = function (beaconId)
	{

	    var cb = function () {
		var resp = BeaconService.removePartner({
                    beaconId: beaconId,
		    partnerId: partnerId

		}, function () {

		    if (resp.error === 0)
		    {
			$scope.updateDisplay();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.message);
		    }
		});
	    };
	    $modal.open({
		templateUrl: 'views/submitmodal.html',
		controller: 'SubmitModalCtrl',
		size: 'sm',
		resolve: {
		    cb: function () {
			return cb;
		    }
		}
	    });
	};
	$scope.pageChanged = function ()
	{
	    var start = ($scope.currentPage - 1) * $scope.itemPerPage;
	    var apiResp = PartnerService.listBeacon({
                partnerId: partnerId,
		start: start,
		length: $scope.itemPerPage

	    }, function () {

		if (apiResp.error === 0) {
		    var beacons = apiResp.data.beacons;
		    $scope.beaconList = beacons;
		    $scope.beacons = [].concat($scope.beaconList);
		    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
		    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
		}
		else
		{
		    ErrorCMDService.displayError(apiResp.message);
		}
	    });
	};
	$scope.updateDisplay = function () {
	    $scope.pageChanged();
	};
	$scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	};
	$scope.init = function () {

	    $scope.$route = $route;
	    //Pagination
	    $scope.totalItems = 0;
	    $scope.itemPerPage = 10;
	    $scope.currentPage = 1;
	    var sessionId = SessionService.getSessionId();
	    if (sessionId)
	    {
		$scope.updateDisplay();
	    } else {
                $scope.updateDisplay();
            }
	};
	$scope.init();
    }
]);