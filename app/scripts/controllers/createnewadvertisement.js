/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('CreateNewAdvertisementCtrl', ['$scope', '$modalInstance', 'cb',
    'AdvertisementService', 'ErrorCMDService',
    function ($scope, $modalInstance, cb,
            AdvertisementService, ErrorCMDService) {

        $scope.init = function ()
        {
            $scope.newAdvertisement = {title: 'title', content: 'content', startTime: 0, endTime: 1};
        };
        $scope.init();


        $scope.ok = function () {
	    $modalInstance.close($scope.newAdvertisement);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };


        $scope.confirm = function () {


            var resp = AdvertisementService.addAdvertisement({
                title: $scope.newAdvertisement.title,
                content: $scope.newAdvertisement.content,
                startTime: $scope.newAdvertisement.startTime,
                endTime: $scope.newAdvertisement.endTime

            }, function () {
                if (resp.error === 0) {
                    $scope.ok();
                    cb();
                }
                else {
                    $scope.showMsg('Add failed');
                }

            });

        }
        ;
    }
]);