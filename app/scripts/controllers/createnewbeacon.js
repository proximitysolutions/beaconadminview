/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('CreateNewBeaconCtrl', ['$scope', '$modalInstance', 'beacon', 'cb',
    'BeaconService', 'ErrorCMDService',
    function ($scope, $modalInstance, beacon, cb,
	    BeaconService, ErrorCMDService) {

	$scope.hasBeacon = (beacon !== undefined);
	$scope.init = function ()
	{
	    $scope.newBeacon = {name: 'name', description: 'description', uuid: '', major: '', minor: '', id: '', latitude: 11, longitude: 12 , enable: 1};
	    if ($scope.hasBeacon)
	    {
		$scope.localBeacon = {
		    name: beacon.beaconName,
		    description: beacon.description,
		    id: beacon.beaconId,
		    uuid: beacon.uuid,
		    major: beacon.bMajor,
		    minor: beacon.bMinor,
                    latitude: beacon.lat,
		    longitude: beacon.lng,
                    enable: beacon.enable
		};
	    }


	};
	$scope.init();


	$scope.ok = function () {
	    $modalInstance.close($scope.localBeacon);
	};
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

	$scope.msgList = [];
	$scope.clearMsg = function () {
	    $scope.msgList = [];
	};
	$scope.showMsg = function (msg) {
	    $scope.msgList = [{type: 'danger', content: msg}];
	};


	$scope.confirm = function () {

	    if ($scope.hasBeacon)
	    {
		var resp = BeaconService.addBeacon({
		    name: $scope.localBeacon.name,
		    description: $scope.localBeacon.description,
		    uuid: $scope.localBeacon.uuid,
		    major: $scope.localBeacon.major,
		    minor: $scope.localBeacon.minor,
                    lat: $scope.localBeacon.latitude,
                    lng: $scope.localBeacon.longitude,
                    enable: $scope.localBeacon.enable,
		    id: $scope.localBeacon.id
		}, function () {
		    if (resp.error === 0)
		    {
			$scope.ok();
			cb();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.error_message);
		    }

		});
	    }
	    else
	    {
		var resp = BeaconService.addBeacon({
		    name: $scope.newBeacon.name,
		    description: $scope.newBeacon.description,
		    uuid: $scope.newBeacon.uuid,
		    major: $scope.newBeacon.major,
		    minor: $scope.newBeacon.minor,
                    lat: $scope.newBeacon.latitude,
                    lng: $scope.newBeacon.longitude,
                    enable: $scope.newBeacon.enable

		}, function () {
		    if (resp.error === 0) {
			$scope.ok();
			cb();
		    }
		    else {
			$scope.showMsg('Add failed');
		    }

		});
	    }
	}
	;
    }
]);