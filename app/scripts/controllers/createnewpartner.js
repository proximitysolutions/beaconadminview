/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('CreateNewPartnerCtrl', ['$scope', '$modalInstance', 'partner', 'cb',
    'PartnerService', 'ErrorCMDService',
    function ($scope, $modalInstance, partner, cb,
	    PartnerService, ErrorCMDService) {

	$scope.hasPartner = (partner !== undefined);
	$scope.init = function ()
	{
	    $scope.newPartner = {name: 'name', username: '', password: '', address: '', enable: 1};
	    if ($scope.hasPartner)
	    {
		$scope.localPartner = {
                    id: partner.partnerId,
		    name: partner.name,
                    address: partner.address,
                    enable: partner.enable
		};
	    }


	};
	$scope.init();


	$scope.ok = function () {
	    $modalInstance.close($scope.localPartner);
	};
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

	$scope.msgList = [];
	$scope.clearMsg = function () {
	    $scope.msgList = [];
	};
	$scope.showMsg = function (msg) {
	    $scope.msgList = [{type: 'danger', content: msg}];
	};


	$scope.confirm = function () {

	    if ($scope.hasPartner)
	    {
		var resp = PartnerService.updatePartner({
                    partnerId: $scope.localPartner.id,
		    name: $scope.localPartner.name,
		    address: $scope.localPartner.address,
                    enable: $scope.localPartner.enable
		}, function () {
		    if (resp.error === 0)
		    {
			$scope.ok();
			cb();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.error);
		    }

		});
	    }
	    else
	    {
		var resp = PartnerService.addPartner({
		    name: $scope.newPartner.name,
                    username: $scope.newPartner.username,
                    password: $scope.newPartner.password,
		    address: $scope.newPartner.address,
                    enable: $scope.newPartner.enable

		}, function () {
		    if (resp.error === 0) {
			$scope.ok();
			cb();
		    }
		    else {
			$scope.showMsg('Add failed');
		    }

		});
	    }
	}
	;
    }
]);