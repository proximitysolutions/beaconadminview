'use strict';

/**
 * @ngdoc function
 * @name upphotoviewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanplayApp
 */
angular.module('myApp')
		.controller('MainCtrl', function ($scope, $route) {
			$scope.$route = $route;
			$scope.things = [];
		});
