/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('PartnerListCtrl', ['$cookieStore', '$scope', '$modal', '$route',
    'PartnerService', 'BeaconService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route,
            PartnerService, BeaconService, ErrorCMDService, SessionService) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;


        $scope.getPartnerUri = function (partnerId)
        {
            return '#partners/' + partnerId;
        };
        
        $scope.viewBeaconList2 = function (partnerId)
        {
            $modal.open({
                templateUrl: 'views/beaconsfrompartner.html',
                controller: 'BeaconsFromPartnerCtrl',
                resolve: {
                    partnerId: function () {
//                        var beacon = resp.data;
//                        beacon.beaconId = beaconId;
                        return partnerId;
                    },
                    cb: function () {
                        return $scope.updateDisplay;
                    }
                }
            });
        }

        $scope.createPartner = function () {

            $modal.open({
                templateUrl: 'views/createnewpartner.html',
                controller: 'CreateNewPartnerCtrl',
                resolve: {
                    partner: function () {
                        return undefined;
                    },
                    cb: function () {
                        return $scope.updateDisplay;
                    }
                }
            });
        };
        $scope.viewPartnerDetail = function (partnerId)
        {
            var resp = PartnerService.getPartner({
                partnerId: partnerId
            }, function ()
            {
                if (resp.error === 0)
                {
                    $modal.open({
                        templateUrl: 'views/partnerdetail.html',
                        controller: 'PartnerDetailCtrl',
                        resolve: {
                            partner: function () {
                                var partner = resp.data;
                                partner.partnerId = partnerId;
                                return partner;
                            },
                            cb: function () {
                                return $scope.updateDisplay;
                            }
                        }
                    });
                }
                else
                {
                    ErrorCMDService.displayError(resp.message);
                }

            });
        };
        $scope.removePartner = function (partnerId)
        {

            var cb = function () {
                var resp = PartnerService.removePartner({
                    partnerId: partnerId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        ErrorCMDService.displayError(resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

        $scope.viewBeaconList = function (partner) {
            $scope.showParnerList = false;
            $scope.showBeaconList = true;
            $scope.showAddBeacon = false;
            $scope.partnerId = partner.partnerId;
            $scope.currentPartner = partner;

            $scope.beaconPageChanged();
        };

        $scope.backToPartnerList = function () {
            $scope.showParnerList = true;
            $scope.showBeaconList = false;
            $scope.showAddBeacon = false;
            $scope.pageChanged();
        };

        $scope.removeBeaconFromPartner = function (beaconId)
        {

            var cb = function () {
                var resp = BeaconService.removePartner({
                    beaconId: beaconId,
                    partnerId: $scope.partnerId

                }, function () {

                    if (resp.error === 0)
                    {
                        $scope.updateDisplay();
                    }
                    else
                    {
                        //ErrorCMDService.displayError(resp.message);
                        alert("Có lỗi trong quá trình remove beacon\n\r error: " + resp.error + " messsage: " + resp.message);
                    }
                });
            };
            $modal.open({
                templateUrl: 'views/submitmodal.html',
                controller: 'SubmitModalCtrl',
                size: 'sm',
                resolve: {
                    cb: function () {
                        return cb;
                    }
                }
            });
        };

        $scope.updatePartnerInfo = function () {
            var resp = PartnerService.updatePartner({
                partnerId: $scope.currentPartner.partnerId,
                name: $scope.currentPartner.name,
                address: $scope.currentPartner.address,
                enable: $scope.currentPartner.enable
            }, function () {
                if (resp.error === 0)
                {
                    //$scope.ok();
                    //cb();
                    alert("Update thành công");
                }
                else
                {
                    //ErrorCMDService.displayError(resp.error);
                    alert("Update thất bại\n\r message: " + resp.message + " error: " + resp.error);
                }

            });
        };
        
        $scope.addBeacon = function () {
            
//	    $modal.open({
//		templateUrl: 'views/addbeacontopartner.html',
//		controller: 'AddBeaconToPartnerCtrl',
//		resolve: {
//		    partnerId: function () {
//			return $scope.partnerId;
//		    },
//		    cb: function () {
//			return $scope.updateDisplay;
//		    }
//		}
//	    });
            $scope.showParnerList = false;
            $scope.showBeaconList = false;
            $scope.showAddBeacon = true;
            $scope.allBeaconPageChanged();
	};
        
        $scope.backToBeaconList = function () {
            $scope.showParnerList = false;
            $scope.showBeaconList = true;
            $scope.showAddBeacon = false;
            $scope.beaconPageChanged();
        };
        
        $scope.adBeaconToList = function (beacon) {
            var index = $scope.beacons.indexOf(beacon);
            //console.log("index: " + index);
            if (index >= 0) {
                //console.log("remove nhe, truoc khi remove size: " + $scope.partners.length);
                $scope.beacons.splice(index, 1);
//                console.log("sau khi remove size: " + $scope.partners.length);
            }
            index = $scope.addedBeacons.indexOf(beacon);
            if(index <0)
                $scope.addedBeacons[$scope.addedBeacons.length] = beacon;
        };
        
        $scope.removeBeaconToList = function (beacon) {
            var index = $scope.addedBeacons.indexOf(beacon);
            //console.log("index: " + index);
            if (index >= 0) {
                //console.log("remove nhe, truoc khi remove size: " + $scope.partners.length);
                $scope.addedBeacons.splice(index, 1);
//                console.log("sau khi remove size: " + $scope.partners.length);
            }
            index = $scope.beacons.indexOf(beacon);
            if(index <0)
                $scope.beacons[$scope.beacons.length] = beacon;
        };
        
        $scope.addBeacons2Partner = function () {
            if ($scope.addedBeacons.length === 0) {
                //ErrorCMDService.displayError("Ban chua chon partner");
                //$scope.showMsg("Ban chua chon beacon");
                alert("Ban chua chon beacon ");
                return;
            }
            //$scope.clearMsg();
            var beaconIds = "";
            for (var i = 0; i < $scope.addedBeacons.length; i++) {
                var beacon = $scope.addedBeacons[i];
                if (beaconIds.length === 0)
                    beaconIds = "" + beacon.beaconId;
                else
                    beaconIds = beaconIds + "," + beacon.beaconId;
            }
//            console.log("partnerIds: " + partnerIds);
            var resp = PartnerService.addBeacons({
                    beaconIds: beaconIds,
                    partnerId: $scope.partnerId
                }, function () {
                    if (resp.error === 0)
                    {
//                        console.log("ok roi nhe " + partnerIds + beaconId);
                        $scope.addedBeacons.splice(0, $scope.addedBeacons.length);
                        $scope.backToBeaconList();
                    }
                    else
                    {
                        alert("Thêm thất bại\n\r message: " + resp.message + " error: " + resp.error);
                        //$scope.showMsg('error: ' + resp.error + ' ' + resp.message);
                    }

                });
        };

        $scope.pageChanged = function ()
        {
            if ($scope.showBeaconList) {
                $scope.beaconPageChanged();
                return;
            }
            if ($scope.showAddBeacon) {
                $scope.allBeaconPageChanged();
                return;
            }
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = PartnerService.listPartner({
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var partners = apiResp.data.partners;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.partnerList = partners;
                    $scope.partners = [].concat($scope.partnerList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    alert("Có lỗi trong quá trình lấy danh sách partner\n\r error: " + apiResp.error + " messsage: " + apiResp.message);
                }
            });
        };

        $scope.beaconPageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = PartnerService.listBeacon({
                partnerId: $scope.partnerId,
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var beacons = apiResp.data.beacons;
                    $scope.beaconList = beacons;
                    $scope.beacons = [].concat($scope.beaconList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    //ErrorCMDService.displayError(apiResp.message);
                    alert("Có lỗi trong quá trình lấy danh sách beacon\n\r error: " + apiResp.error + " messsage: " + apiResp.message);
                }
            });
        };
        
        $scope.allBeaconPageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = BeaconService.listBeacon({
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var beacons = apiResp.data.beacons;
                    $scope.beaconList = beacons;
                    $scope.beacons = [].concat($scope.beaconList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    alert("Có lỗi trong quá trình lấy danh sách beacon\n\r error: " + apiResp.error + " messsage: " + apiResp.message);
                }
            });
        };

        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.init = function () {

            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            $scope.showParnerList = true;
            $scope.showBeaconList = false;
            $scope.showAddBeacon = false;
            
            $scope.addedBeaconList = [];
            $scope.addedBeacons = [].concat($scope.addedBeaconList);
            
            var sessionId = SessionService.getSessionId();
            if (sessionId)
            {
                $scope.updateDisplay();
            } else {
                $scope.updateDisplay();
            }
        };
        $scope.init();
    }
]);