'use strict';
var app = angular.module('myApp');
app.controller('UserListCtrl', ['$scope', '$modal', '$route',
    'UserService', 'ErrorCMDService', 'SessionService',
    function ($scope, $modal, $route,
	    UserService, ErrorCMDService, SessionService) {

	$scope.viewUserDetail = function (id) {

	    var resp = UserService.getUser({userId: id},
	    function () {
		if (resp.error_code === 0)
		{
		    $modal.open({
			templateUrl: 'views/userdetail.html',
			controller: 'UserDetailCtrl',
			resolve: {
			    cb: function () {
				return undefined;
			    },
			    user: function () {
				var user = resp.data;
				user.viewAdmin = true;
				user.id = id;
				return user;
			    }
			}
		    });

		}
		else
		{
		    ErrorCMDService.displayError(resp.error_message);
		}
	    });


	};

	$scope.addUser = function () {

	    $modal.open({
		templateUrl: 'views/userdetail.html',
		controller: 'UserDetailCtrl',
		resolve: {
		    cb: function () {
			return $scope.updateDisplay;
		    },
		    user: function () {
			return undefined;
		    }
		}
	    });
	};
	$scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	};


	$scope.pageChanged = function () {
	    var start = ($scope.currentPage - 1) * ($scope.itemPerPage + 1);
	    var apiResp = UserService.listUser({
		start: start,
		limit: $scope.itemPerPage

	    }, function () {
		if (apiResp.error_code === 0) {
		    var users = apiResp.data.users;
		    for (var i = 0; i < users.length; ++i) {
			var d = new Date(users[i].createTime * 1000);
			users[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
			d = new Date(users[i].lastUpdate * 1000);
			users[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
		    }
		    $scope.userList = users;
		    $scope.users = [].concat($scope.userList);
		    $scope.totalItems = apiResp.data.total;
		    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
		}
		else
		{
		    ErrorCMDService.displayError(apiResp.error_message);
		}
	    });
	};
	$scope.updateDisplay = function ()
	{
	    $scope.pageChanged();
	};

	$scope.init = function () {

	    $scope.$route = $route;

	    //Pagination
	    $scope.totalItems = 0;
	    $scope.itemPerPage = 10;
	    $scope.currentPage = 1;

	    var sessionId = SessionService.getSessionId();
	    if (sessionId)
	    {
		$scope.updateDisplay();
	    }

	};
	$scope.init();
    }
]);

