/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
angular.module('myApp')
	.factory('AppService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {

		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();
		return $resource(null,
			{
			    sessionId: sessionId
			},
		{
		    addApp: {
			method: 'GET',
			url: apiUrl + '?method=beacon_app.addapp'
		    },
		    getApp: {
			method: 'GET',
			url: apiUrl + '?appId=:appId&method=beacon_app.getapp'
		    },
		    updateApp: {
			method: 'GET',
			url: apiUrl + '?appId=:appId&name=:name&url=:url&enable=:enable&method=beacon_app.updateapp'
		    },
		    removeApp: {
			method: 'GET',
			url: apiUrl + '?appId=:appId&method=beacon_app.remove'
		    },
		    listApp: {
			method: 'GET',
			url: apiUrl + '?&method=beacon_app.getlist'
		    }

		});
	    }]);