/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
angular.module('myApp')
	.factory('BeaconService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {

		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();
		return $resource(null,
			{
			    sessionId: sessionId
			},
		{
		    addBeacon: {
			method: 'GET',
			url: apiUrl + '?method=beacon.put'
		    },
		    getBeacon: {
			method: 'GET',
			url: apiUrl + '?beaconId=:beaconId&method=beacon.getbeacon'
		    },
		    updateBeacon: {
			method: 'GET',
			url: apiUrl + '?uuid=:uuid&major=:major&minor=:minor&method=beacon.update' +
				'&description=:description' + '&name=:name'
		    },
		    removeBeacon: {
			method: 'GET',
			url: apiUrl + '?beaconId=:beaconId&method=beacon.remove'
		    },
		    listBeacon: {
			method: 'GET',
			url: apiUrl + '?&method=beacon.getlist'
		    },
		    listPartner: {
			method: 'GET',
			url: apiUrl + '?&method=beacon.getpartners'
		    },
                    addPartner:  {
			method: 'GET',
			url: apiUrl + '?&method=beacon.addpartner'
		    },
                    addPartners:  {
			method: 'GET',
			url: apiUrl + '?&method=beacon.addpartners'
		    },
                    removePartner:  {
			method: 'GET',
			url: apiUrl + '?method=beacon.removepartner'
		    }

		});
	    }]);
